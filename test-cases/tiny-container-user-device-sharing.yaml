metadata:
  name: tiny-container-user-device-sharing
  format: "Apertis Test Definition 1.0"
  image-types:
    tiny-lxc: [ armhf-internal, armhf, arm64, amd64 ]
  image-deployment:
    - LXC
  type: functional
  exec-type: automated
  priority: medium
  maintainer: "Apertis Project"
  description: "Test of device sharing between host and Tiny unprivileged
                container started as user"

  expected:
    - "Test command should report \"pass\"."

install:
  git-repos:
    - url: https://gitlab.apertis.org/infrastructure/tiny-image-recipes.git
      branch: 'apertis/v2022dev0'

run:
  steps:
    - "# Enter test directory:"
    - cd tiny-image-recipes
    - "# Ensure we allow user mapping:"
    - sysctl -w kernel.unprivileged_userns_clone=1
    - "# Setup the AppArmor profile for container:"
    - sed s/__NAMESPACE_PLACEHOLDER__/lxc-apertis-tiny-userns/g lxc/lxc-tiny-connectivity-profile-template | apparmor_parser -qr
    - "# Ensure we have loop device:"
    - modprobe loop
    - "# Create the random file and map it to loop0 device on host:"
    - dd if=/dev/urandom of=/var/test.img bs=1M count=1
    - losetup /dev/loop0 /var/test.img
    - "# Make sure user have correct mappings for test:"
    - usermod --add-subuids 1000-1000 user
    - usermod --add-subuids 100000-165535 user
    - usermod --add-subgids 6-6 user
    - usermod --add-subgids 100000-165535 user
    - "# Add user to group 'disk' for accessing to '/dev/loop0' device:"
    - usermod -a -G disk user
    - "# Check that a simple loop device created on the host can be shared with the container and accessed from inside it:"
    - sudo -u user -H lavatests/test-device-sharing --ospack "$OSPACK" -t lxc/lxc-tiny-connectivity --aa-namespace "lxc-apertis-tiny-userns"
    - "# Release the loop0 device on host after the test:"
    - losetup -d /dev/loop0

parse:
  pattern: "(?P<test_case_id>.*-*):\\s+(?P<result>(pass|fail))"
